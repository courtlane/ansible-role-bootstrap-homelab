bootstrap-homelab
=========

This role is designed to simplify the setup process for virtual machines within a homelab environment. It handles tasks such as installing common packages, configuring sshd, and managing ssh keys.

Compatibility
------------
This role is compatible with the following operating systems:
- Debian >= 11 (bullseye)
- Ubuntu >= 20.04 (focal)

Requirements
------------

- None

Role Variables
--------------

The default values for the variables are set in `defaults/main.yml`:
```yml
qemu_guest_agent: yes # Wether Qemu guest agent should be installed and started
timezone: Europe/Berlin
root_password: "" # Optional: Set root password by providing a encrypted password hash (generate via openssl passwd -5)
ssh_user: root
ssh_keys: [] # List with SSH-Keys
core_packages:
  - vim
  - bash-completion
  - dnsutils
  - sudo
  - python3
  - python3-pip
  - gnupg
  - apt-transport-https
  - ca-certificates
  - openssh-server

additional_packages: [] # Optional: list with additional packages to install
#  - docker.io
#  - docker-compose

additional_disks: [] # Optional: list with additional disks to mount and create filesystem
#  - device: /dev/vdb # Device name
#    fstype: ext4 # Filesystem to create on disk. Supported filesystems are ext4, xfs.
#    directory: /opt/vault # Mountpoint
#    mountoptions: noatime # Mountoptions in /etc/fstab

dist_upgrade: no # Optional: Specifies whether Ansible should upgrade all packages and perform apt dist-upgrade if set to 'yes'
```

Example Playbook
----------------
```yml
- hosts: all
  become: yes
  roles:
    - role: bootstrap-homelab
```

License
-------

GPLv3

Author Information
------------------

Oleg Franko 
