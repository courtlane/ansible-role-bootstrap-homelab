# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        if [ -f "$rc" ]; then
            . "$rc"
        fi
    done
fi
unset rc

#---------------------------------------------------------
# History config
#---------------------------------------------------------

# Avoid duplicates..
export HISTCONTROL=ignoredups:erasedups

# Append history entries..
shopt -s histappend

# History size
export HISTSIZE=100000

# Prevent history appearance for commands starting with space
export HISTIGNORE=' *'

#---------------------------------------------------------
# Aliases
#---------------------------------------------------------

alias vi='vim'
alias ls='ls --color'
alias ll='ls -l --color'
alias la='ls -la --color'
alias dps='docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}\t{{.CreatedAt}}\t{{.Size}}"'

#---------------------------------------------------------
# Custom variables
#---------------------------------------------------------

# Preferred editor
export EDITOR='vim'
